// JSON Objects
/*
- It stands for Javascript Object Notation
- Also used in other programming language
- Core JSS has a built in JSON object that contains methods
for parsing JSON Objects and converting strings into Javascript
objects
- Javascript Objects are not to be confused with JSON
- JSON is used for serializing different data types into bytes
- Serialization is the process of converting data into a series
of bytes for easier transmission / transfer of information
- A byte is a unit of data that is eight binary (1 and 0) that is used to represent a character(letters, numbers, or typographic symbols)
- Syntax: 
	{
		"propertyA": "propertyA",
		"propertyB": "propertyB"
	}
*/

// JSON
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// JSON, Array
// "cities": [
// 	{"city": "Quezon City", "province": "Metro Manila", 	"country": "Philippines"},
// 	{"city": "Manila City", "province": "Metro Manila", 	"country": "Philippines"},
// 	{"city": "Makati City", "province": "Metro Manila", 	"country": "Philippines"},
// ]
// console.log(cities[0].city)

// PARSE: Converting JSON string into objects
let batchesJSON = '[{"batchName": "BatchX"}, {"batchName": "BatchY"}]';
console.log('Result from parse method:')
console.log(JSON.parse(batchesJSON));
//console.log(batchesJSON[0].batchName)

let stringifiedObject = `{"name": "Eric", "age": "9", "address": {"city": "Bonifacio Global City", "country": "Philippines" }}`
console.log(JSON.parse(stringifiedObject));

// STRINGIFY: Convert Objects into String(JSON)
// JSON.stringify
let data = {
	name: 'Gabryl',
	age: 61,
	address: {
		city: 'New York City',
		country: 'USA'
	}
}
console.log(data);
console.log(typeof data);

let stringData = JSON.stringify(data)
console.log(stringData);
console.log(typeof stringData);
